const crypto = require('crypto'); const algorithm = 'aes-256-cbc'; const key = crypto.randomBytes(32); const iv = crypto.randomBytes(16);
  
function encrypt(text) {
    let cipher = 
        crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return { iv: iv.toString('hex'),
        encryptedData: encrypted.toString('hex') };
}
  
function decrypt(text) {
    let iv = Buffer.from(text.iv, 'hex');
    let encryptedText =
        Buffer.from(text.encryptedData, 'hex');
    let decipher = crypto.createDecipheriv(
            'aes-256-cbc', Buffer.from(key), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return {decryptedData: decrypted.toString()};
}

let msg = "Bitcoin P2P e-cash paper Satoshi Nakamoto satoshi_at_vistomail.com Fri Oct 31 14:10:00 EDT 2008 Previous message: Fw: SHA-3 lounge Messages sorted by: [ date ][ thread ][ subject ][ author ] I've been working on a new electronic cash system that's fully peer-to-peer, with no trusted third party. The paper is available at: http://www.bitcoin.org/bitcoin.pdf The main properties: Double-spending is prevented with a peer-to-peer network. No mint or other trusted parties. Participants can be anonymous. New coins are made from Hashcash style proof-of-work. The proof-of-work for new coin generation also powers the network to prevent double-spending. Bitcoin: A Peer-to-Peer Electronic Cash System"

// encrypt output
var output = encrypt(msg);
console.log(output);
// decrypt output
console.log(decrypt(output));

