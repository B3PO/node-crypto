const { createHash } = require('node:crypto');

let data = "Hashing this string to infinity. \n";
let count = 100;
const hash = createHash('sha256');
for (let i=0; i<=count; i++){
    hash.update(data)
    str = `Block: ${i} is minted \n`
    console.log(str);
    data += str;
}
console.log(hash.digest('hex'));
