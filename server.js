const cryptothis = require('./crypto');

let msg = "This is a secret message";
let enc = cryptothis.encrypt(msg);
console.log(enc);
let dec = cryptothis.decrypt(enc);
console.log(dec);