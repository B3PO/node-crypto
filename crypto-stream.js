const crypto = require('crypto');
const fs = require('fs');

const algorithm = 'aes-256-ctr';
const secretKey = 'vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3';
const iv = crypto.randomBytes(16);

// input file
console.log('---Read File.txt---')
const r = fs.createReadStream('file.txt');
console.log();

// write file
const w = fs.createWriteStream('outfile.txt');

// encrypt content
console.log('---Encrypt Stream---')
const encrypt = crypto.createCipheriv(algorithm, secretKey, iv);
console.log(encrypt);
console.log();

// decrypt content
console.log('---Decrypt Stream---')
const decrypt = crypto.createDecipheriv(algorithm, secretKey, iv);
console.log(decrypt);
console.log();


// start pipe
r.pipe(encrypt)
    //.pipe(decrypt)
    .pipe(w);



    