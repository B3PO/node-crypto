const fs = require('fs');
const { createReadStream } = require('node:fs');
const { stdout } = require('node:process');
const { createHash } = require('node:crypto');

// create a file to hash forever
let data_str = "I'm going to hash this, just because. \n";
let count = 99;
for (let i=0; i<=count; i++){
    fs.writeFile('data.txt', data_str, {flag: 'a+'}, (err) => {
        if(err) throw err;
    });
}
console.log('Data written to file');

//create hash
const hash = createHash('sha256');

const input = createReadStream('data.txt');
input.pipe(hash).setEncoding('hex').pipe(stdout);